from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from django.apps import apps

class UnitTestStory7(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_function_story7(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_index_is_using_correct_html(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landing_page_is_written(self):
        self.assertIsNotNone(index)
$("#search").keyup( function() {
    var isi = $("#search").val();
    $.ajax({
        url: "/buku?q=" + isi,
        success: function(hasil) {
            var obj_hasil = $('#table');
            obj_hasil.empty();
            var tabel = "<tr> <th>No</th> <th id='judul'>Judul</th> <th>Gambar</th> </tr>"
            obj_hasil.append(tabel);

            for (i = 0; i < hasil.items.length; i++) {
                var j = i + 1;
                var tmp_title = hasil.items[i].volumeInfo.title;
                var tmp_thumbnails = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
                obj_hasil.append('<tr> <td>' + j + '</td> <td><b>' + tmp_title + '</b></td> <td> <img src=' + tmp_thumbnails + '> </td> </tr>');
            }
        }
    });
});